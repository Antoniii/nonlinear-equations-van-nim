# nim c -r dicho.nim

import math

let eps = 0.001

var 
    a = 3.0
    b = 4.0
    x: float64

proc f(x: float64): float64 =
    return pow(x,2)*cos(2*x)+1

while abs(b-a) > eps: 
    x = (a+b)/2
    echo "x = ", x
    if f(a)*f(b) < 0:
        a = x
    else:
        b = x     

echo "root is: ", x # 3.9697265625


# Numerical solutions

# x ≈ ± 5.48114120661372...
# x ≈ ± 3.95891449098817...
# x ≈ ± 2.25743797127130...
# x ≈ ± 1.18320719655542...
