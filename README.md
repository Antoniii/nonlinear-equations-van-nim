> sudo pacman -S nim  
> nim c -r dicho.nim  

![](https://gitlab.com/Antoniii/nonlinear-equations-van-nim/-/raw/main/image-18.jpg)

## Sources

* [Nim basics](https://narimiran.github.io/nim-basics/#_declaring_a_procedure)
* [Nim Standard Library](https://nim-lang.org/docs/lib.html)
